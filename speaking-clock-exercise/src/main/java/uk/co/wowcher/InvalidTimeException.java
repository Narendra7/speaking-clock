package uk.co.wowcher;

public class InvalidTimeException extends Exception {
private static final long serialVersionUID = -5600057890022102537L;
	
	public InvalidTimeException(final String e) {
		super(e);
	}
}
