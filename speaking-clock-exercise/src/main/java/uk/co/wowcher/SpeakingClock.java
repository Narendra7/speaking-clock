package uk.co.wowcher;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class SpeakingClock {

	public static void main(String[] args) throws InvalidTimeException {
		Scanner s = new Scanner(System.in);
		System.out.println("Please provide the time in HH:mm format only: ");
		DateTimeFormatter parseFormat = DateTimeFormatter.ofPattern("HH:mm");
		String timeString = s.nextLine();
		try{
			 validate(timeString);
			 LocalTime time = LocalTime.parse(timeString, parseFormat);
			    System.out.println(time);
			    
			    int hrs = time.getHour();
				int min = time.getMinute();
				System.out.println("Hours: "+hrs+" " +"Minutes: "+min);
				if(hrs == 12 && min == 0)
				{
				    System.out.println("It's Midday");
				}
				else if(hrs == 0 && min == 0)
				{
				    System.out.println("It's Midnight");
				}
				else
				{
				    String[] ones = new String[] {
		            "", "one", "two",   "three", "four",
		            "five", "six", "seven", "eight", "nine"};
		            
		            String[] teens = new String[] {
		             "ten",      "eleven",  "twelve",
		            "thirteen",  "fourteen", "fifteen", "sixteen",
		            "seventeen", "eighteen", "nineteen"};
		            
		            String[] doubles = new String[] {
		            "",      "",      "twenty",  "thirty", "forty",
		            "fifty"};
		            
		            String name = "It's ";
		            int d = hrs % 10;
		            int c = hrs / 10;
		            if (c == 0 && d == 0) 
		                name = name + " zero";
		                
		            else if(c == 0) 
		                name = name + " " + ones[d];
		                
		            else if(c == 1) 
		                name = name + " " + teens[hrs-10];
		            
		            else
		            {
		                name = name + " " + doubles[c];
		                if(d!=0)
		                name = name + " " + ones[d];
		                
		            }
		            int b = min % 10;
				    int a = min / 10;
				        
				    if(a == 0 && b == 0)
				    {
				        
				    }
				    else if (a == 0) 
				        name = name + " " + ones[b];
				        
				    else if(a == 1)
				        name = name + " " + teens[min-10];
				        
				    else
				    {
				        name = name + " " + doubles[a];
				        if(b!=0)
				            name = name + " " + ones[b];
				    }
				  
				    System.out.println(name);
				   
				    
				    
				}
		   }catch(InvalidTimeException e){
			   System.out.println(e.getMessage());
			   } 
		 finally{
			 try {
				Thread.sleep(1 * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 main(args);
		 }
	   
	}
	


		private static void validate(final String time) throws InvalidTimeException {
			if(time.isEmpty()) { 
				throw new InvalidTimeException("Time is blank");
				
			}
			if(time.length() != 5 || !time.contains(":")) {
				throw new InvalidTimeException("Invalid Time format, should be HH:mm");
			}
			String[] timeSplit = time.split(":");
			Integer hours = new Integer(0);
			try {
				hours = Integer.parseInt(timeSplit[0]);
			} catch(NumberFormatException e) {
				throw new InvalidTimeException("Invalid Hours");
			}
			if(hours < 0 || hours > 23) {
				throw new InvalidTimeException("Invalid Hours");
			}
			Integer minutes = new Integer(0);
			try {
				minutes = Integer.parseInt(timeSplit[1]);
			} catch(NumberFormatException e) {
				throw new InvalidTimeException("Invalid Minutes");
			}
			if(minutes < 0 || minutes > 59) {
				throw new InvalidTimeException("Invalid Minutes");
			}
		}
	}
